import sqlite3 as sql


dbName = 'init.sqlite'
def write_db(fd, params=None, variable=None):
    con = sql.connect(dbName)
    con.row_factory = sql.Row
    cur = con.cursor()
    with open(fd) as fd:
        try:
            sqlrequest=fd.read()
            if (variable):
                sqlrequest=sqlrequest.replace('variable', variable)
            cur.execute(sqlrequest, params)
            con.commit()
        except Exception as e:
            con.rollback()
            raise
    con.close()
    return cur.lastrowid

def request_db(fd, params=()):
    con = sql.connect(dbName)
    con.row_factory = lambda c, r: dict([(col[0], r[idx]) for idx, col in enumerate(c.description)])
    #sql.Row
    cur = con.cursor()
    with open(fd) as fd:
        try:
            cur.execute(fd.read(), params)
            return cur.fetchall()
        except Exception as e:
            con.rollback()
            raise
    con.close()


