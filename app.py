import os
from flask import Flask, abort
from flask import render_template, send_from_directory
from flask import redirect, url_for
from flask import request
import requests, split, json
from dbaccess import DBAccess
from flask import jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/character/<characterId>", methods=['DELETE'])
def deleteCharacter(characterId):
    DBAccess.write_db('sql/delete_character.sql', (characterId,))
    return json.dumps({'success':True}), 202, {'ContentType':'application/json'}

@app.route("/character/<characterId>", methods=['GET'])
def getCharacter(characterId):
    data = DBAccess.request_db('sql/get_character.sql', (characterId,))
    response=jsonify(data[0])
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route("/character/<characterId>", methods=['put'])
def updateCharacter(characterId):
    data = DBAccess.request_db('sql/get_character.sql', (characterId,))
    params = request.args
    action = params.get('action', data.action)
    actualaction = params.get('actualaction', data.actualaction)
    power = params.get('power', data.power)
    actualpower = params.get('actualpower', data.actualpower)
    initiative = params.get('initiative', data.Initiative)
    DBAccess.write_db('sql/update_character.sql', ( action, actualaction, power, actualpower, initiative, characterId))
    response=jsonify(data)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route("/rpg/<rpgId>", methods=['GET'])
def getRpg(rpgId):
    params = request.args
    limit = params.get('limit', 10)
    skip = params.get('skip', 0)
    search_string = '%'+params.get('searchString', '')+'%'
    characters = DBAccess.request_db('sql/search_characters.sql', (search_string, rpgId, limit, skip))
    characterIDs = map(lambda item: item.get('id') , characters)
    rpg = DBAccess.request_db('sql/get_rpg.sql', (rpgId,))
    response=jsonify({'id':rpg[0].get('id'), 'name':rpg[0].get('name'), 'powername':rpg[0].get('powername'), 'characters':characterIDs})
   # response=jsonify({'character':data})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route("/rpg", methods=['GET'])
def getRpgs():

    params = request.args
    limit = params.get('limit', 10)
    skip = params.get('skip', 0)
    search_string = '%'+params.get('searchString', '')+'%'
    data = DBAccess.request_db('sql/search_rpgs.sql', (search_string, limit, skip))
    response=jsonify(data)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route("/rpg", methods=['POST'])
def addRpg():
    data = request.data
    params = json.loads(data)
    name = params.get('name', '')
    powername = params.get('powername', '')
    maxinit = params.get('maxinit', 0)
    rpgId = DBAccess.write_db('sql/add_rpg.sql', (name, powername, maxinit))
    return json.dumps({'id': rpgId}), 200, {'ContentType':'application/json'}

@app.route("/rpg/<rpgId>", methods=['DELETE'])
def deleteRpg(rpgId):
    DBAccess.write_db('sql/delete_rpg.sql', (rpgId,))
    return json.dumps({'success':True}), 202, {'ContentType':'application/json'}

@app.route("/character", methods=['POST'])
def addCharacter():
    data = request.data
    params = json.loads(data)
    name = params.get('name', '')
    trigram = params.get('trigram', '')
    initiative = params.get('initiative', 15)
    power = params.get('power', 0)
    action = params.get('action', 0)
    friend = params.get('friend', 1)
    rpgid = params.get('rpgid', 1)
    characterId = DBAccess.write_db('sql/add_character.sql', (name, trigram, initiative, power, power, action, action, friend, rpgid))
    return json.dumps({'id': characterId}), 200, {'ContentType':'application/json'}


@app.route("/character/<charid>/<variable>/<newvalue>", methods=['put'])
def updateCharacterVariable(variable, charid, newvalue):
    DBAccess.write_db('sql/update_character_variable.sql', (int(newvalue), charid), variable)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


if __name__ == "__main__":
    app.run(host="localhost")
