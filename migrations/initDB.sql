
drop table if exists rpg cascade;
CREATE TABLE rpg
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    powername VARCHAR(100),
    initiativeMax integer
);

drop table if exists character;
CREATE TABLE character
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    trigram VARCHAR(100),
    rpgid integer not null,
    FOREIGN KEY (rpgid) REFERENCES rpg(id),
    action integer,
    actualaction integer,
    power integer,
    actualpower integer,
    wait boolean DEFAULT false,
    friend  boolean DEFAULT false
);
